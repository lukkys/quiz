package com.example.meotxe_leqcia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class YourPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_page);
    }
}