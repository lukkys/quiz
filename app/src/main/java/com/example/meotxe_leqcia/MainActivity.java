package com.example.meotxe_leqcia;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private EditText email;
    private EditText firstPassword;
    private EditText secondPassword;
    private Button registerButton;

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = findViewById(R.id.email);
        firstPassword = findViewById(R.id.firstPassword);
        secondPassword = findViewById(R.id.secondPassword);
        registerButton = findViewById(R.id.registerButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mail = email.getText().toString();
                String firstPwd = firstPassword.getText().toString();
                String secPwd = secondPassword.getText().toString();
                if(TextUtils.isEmpty(mail) || TextUtils.isEmpty(firstPwd) || TextUtils.isEmpty(secPwd)){
                    Toast.makeText(MainActivity.this, "Empty!", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    if(firstPwd.equals(secPwd) && firstPwd.length()>=8){
                        firebaseAuth.createUserWithEmailAndPassword(mail,firstPwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    Intent intent = new Intent(MainActivity.this, YourPageActivity.class);
                                    startActivity(intent);

                                }
                            }
                        });
                    }
                }




            }
        });

    }
}